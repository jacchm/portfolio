package com.timbuchalka;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        int h = 0;

        boolean isFirstHeightLoop = true;
        while (true) {
            try {
                if (isFirstHeightLoop) {
                    System.out.println("Please, enter the height of the Hanoi tower:");
                } else {
                    System.out.println("You have entered wrong data. Please, enter the height of the Hanoi tower:");
                }
                h = scanner.nextInt();
                break;
            } catch (InputMismatchException e) {
                scanner.nextLine();
                isFirstHeightLoop = false;
                continue;
            }
        }

        Tower tower_1 = new Tower(h, true);
        Tower tower_2 = new Tower(h, false);
        Tower tower_3 = new Tower(h, false);


        int winningSum = tower_1.getSumOfBlocks();
        int controlSum = 0;


        while (controlSum != winningSum) {

            controlSum = 0;

            int moveFrom = 0;
            int moveTo = 0;



            while (true) {

                System.out.println();
                tower_1.towerPrinting();
                tower_2.towerPrinting();
                tower_3.towerPrinting();

                boolean firstTimeLoop = true;
                while (true) {
                    try {
                        if (firstTimeLoop) {
                            System.out.println("Please, enter the number (1, 2 or 3) of tower from which " +
                                    "you want to take a block:");
                        } else {
                            System.out.println("Please, re-enter the number (1, 2 or 3) of tower from which " +
                                    "you want to take a block:");
                        }
                        moveFrom = scanner.nextInt();
                        if (moveFrom >= 1 && moveFrom <= 3) {
                            break;
                        } else {
                            firstTimeLoop = false;
                            continue;
                        }
                    } catch (InputMismatchException e) {
                        scanner.nextLine();
                        firstTimeLoop = false;
                        continue;
                    }
                }


                firstTimeLoop = true;
                while (true) {
                    try {
                        if (firstTimeLoop) {
                            System.out.println("Please, enter the number of tower (1, 2 or 3) " +
                                    "where you want to put a block:");
                        } else {
                            System.out.println("Please, re-enter the number of tower (1, 2 or 3) " +
                                    "where you want to put a block:");
                        }
                        moveTo = scanner.nextInt();
                        if (moveTo >= 1 && moveTo <= 3) {
                            break;
                        } else {
                            firstTimeLoop = false;
                            continue;
                        }

                    } catch (InputMismatchException e) {
                        scanner.nextLine();
                        firstTimeLoop = false;
                        continue;
                    }
                }

                int cutBlock = 0;
                int tempValueToBeMoved = 0;

                if (moveFrom == 1) {
                    cutBlock = tower_1.cutTheTopBlockNumber();
                    tempValueToBeMoved = tower_1.cutTheTopBlockValue();
                } else if (moveFrom == 2) {
                    cutBlock = tower_2.cutTheTopBlockNumber();
                    tempValueToBeMoved = tower_2.cutTheTopBlockValue();
                } else {
                    cutBlock = tower_3.cutTheTopBlockNumber();
                    tempValueToBeMoved = tower_3.cutTheTopBlockValue();
                }

                if (tempValueToBeMoved == -1) {
                    System.out.println("Invalid move. Try again. 000");
                    System.out.println();
                    continue;
                }

                if (moveTo == 1) {
                    if (!tower_1.addTheTopBlock(tempValueToBeMoved)){
                        System.out.println("Invalid move. Try again. 001");
                        System.out.println();
                        continue;
                    }
                } else if (moveTo == 2) {
                    if (!tower_2.addTheTopBlock(tempValueToBeMoved)){
                        System.out.println("Invalid move. Try again. 002");
                        System.out.println();
                        continue;
                    }
                } else if (moveTo ==3){
                    if (!tower_3.addTheTopBlock(tempValueToBeMoved)){
                        System.out.println("Invalid move. Try again. 003");
                        System.out.println();
                        continue;
                    }
                }

                // REMOVAL OF THE MOVED BLOCK
                if (moveFrom == 1) {
                    tower_1.removalOfTheTopBlock(cutBlock);
                } else if (moveFrom == 2) {
                    tower_2.removalOfTheTopBlock(cutBlock);
                } else {
                    tower_3.removalOfTheTopBlock(cutBlock);
                }

                break;
            }

            // CHECK OF THE WINNING CONDITION
            controlSum = tower_3.sumOfTheBlocks();


        }

        System.out.println();
        tower_1.towerPrinting();
        tower_2.towerPrinting();
        tower_3.towerPrinting();

        System.out.println("USER, YOU WIN!!!!");
        System.out.println("USER, YOU WIN!!!!");
        System.out.println("USER, YOU WIN!!!!");

    }


}

