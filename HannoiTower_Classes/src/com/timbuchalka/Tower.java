package com.timbuchalka;

public class Tower {

    private int height;
    private int[] towerBlocks;
    private boolean isFirst;
    private int sumOfBlocks;

    public Tower(int height, boolean isFirst) {
        this.height = height;
        this.isFirst = isFirst;
        this.towerBlocks = new int[height];
        this.sumOfBlocks = 0;
        if (this.isFirst) {
            for (int i = 0; i < this.towerBlocks.length; i++) {
                this.towerBlocks[i] = i * 2 + 1;
                this.sumOfBlocks += this.towerBlocks[i];
            }
        }

    }

    public int getSumOfBlocks() {
        return this.sumOfBlocks;
    }

    public void towerPrinting() {

        int printingArrayWidth = 2 * this.height - 1;

        for (int r = 0; r <= (this.towerBlocks.length - 1); r++) {

            if (r == 0) {
                for (int space = (printingArrayWidth - this.towerBlocks[r]) / 2; space >= 1; space--) {
                    System.out.print(" ");
                }
                System.out.println("|");
            }

            for (int space = (printingArrayWidth - this.towerBlocks[r]) / 2; space >= 1; space--) {
                System.out.print(" ");
            }

            if (this.towerBlocks[r] == 0) {
                System.out.print("|");
            } else {
                for (int star = 1; star <= this.towerBlocks[r]; star++) {
                    System.out.print("*");
                }
            }

            System.out.println();

        }
        System.out.println();
    }




    public int cutTheTopBlockNumber() {

        for (int i = 0; i < this.towerBlocks.length; i++) {
            if (this.towerBlocks[i] != 0) {
                return i;
            }
        }
        return -1;

    }



    public int cutTheTopBlockValue() {
        for (int i = 0; i < this.towerBlocks.length; i++) {
            if (this.towerBlocks[i] != 0) {
                return this.towerBlocks[i];
            }
        }
        return -1;
    }



    public void removalOfTheTopBlock(int number) {
        this.towerBlocks[number] = 0;
    }



    public boolean addTheTopBlock(int valueToBePasted) {

        for (int i = this.towerBlocks.length - 1; i >= 0; i--) {

            if ((i == this.towerBlocks.length - 1 && this.towerBlocks[i] == 0) ||
                    ((this.towerBlocks[i] == 0) && (this.towerBlocks[i + 1] > valueToBePasted))) {
                this.towerBlocks[i] = valueToBePasted;
                return true;
            }
        }
        return false;

    }


    public int sumOfTheBlocks(){
        int sum = 0;
        for (int i = 0; i<this.towerBlocks.length; i++){
            sum = sum + this.towerBlocks[i];
        }
        return sum;
    }









}